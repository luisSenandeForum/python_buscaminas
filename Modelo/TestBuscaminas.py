import unittest
from Modelo.BuscaminasException import *
from Modelo.Buscaminas import *


class TestBuscaminas(unittest.TestCase):
    def inicializacion(self):
        a = [[0, 0],[ 2, 4]]
        juego = Buscaminas()
        juego.init_coord(10, 10, a)

        self.assertTrue(juego.hay_mina(0, 0))
        self.assertTrue(juego.hay_mina(2, 4))

    def inicializacion_exceso_minas(self):
        with self.assertRaises(BuscaminasException):
            juego = Buscaminas()
            juego.init_minas(10, 10, 100)

    def test_exceso_minas(self):
        dimension = 3;
        juego = Buscaminas()
        juego.init_minas(dimension, dimension, dimension * dimension - 1)

        #self.assertEquals(dimension, juego.get_altura())
        #self.assertEquals(dimension, juego.get_anchura())

        self.assertEquals(dimension * dimension - 1, juego.get_total_minas())

        num_minas = 0;
        for i in range(juego.get_altura()-1):
            for j in range(juego.get_anchura()-1):
                if juego.hay_mina(i, j):
                    num_minas += 1

        self.assertEquals(dimension * dimension-1, num_minas)


if __name__ == '__main__':
    unittest.main()
