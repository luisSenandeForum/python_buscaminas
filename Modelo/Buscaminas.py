
from Modelo.Celda import *
from Modelo.BuscaminasException import *
from random import randrange


class Buscaminas:
    niveles = {'principiante':[8,8,10],'intermedio':[16,16,49],'experto':[16,30,99]}
    def init_nivel(self, nivel):
        self.init_minas(self.niveles[nivel][0], self.niveles[nivel][1], self.niveles[nivel][2])

    def init_minas(self, ancho, alto, n_minas):
        self._altura = alto
        self._anchura = ancho
        self._total_minas = self._minas_restantes = n_minas
        if self.comprueba_tablero():
            self.generar_tablero()

    def init_coord(self, ancho, alto, coord_minas):
        for crd in coord_minas:
            if crd[0] >= self.get_anchura() or crd[0] < 0 or crd[1] >= self.get_altura() or crd[1] < 0:
                raise BuscaminasException()
        self._coord_minas = coord_minas
        self.init_minas(ancho, alto, len(coord_minas))

    def comprueba_tablero(self):
        if self.get_total_minas() <= 0:
            raise BuscaminasException("Sin minas que colocar")
        if (self.get_altura() * self.get_anchura()) <= self.get_total_minas():
            raise BuscaminasException("Explotaste por exceso de minas!!!!!!")
        if self.get_altura() < 2 and self.get_anchura() < 2:
            raise BuscaminasException("Pocas celdas!!!!!!")

        return True

    def gen_tab(self):
        for x in self._tablero:
            for y in self._tablero[x]:
                self._tablero[x][y] = Celda()

    def generar_tablero(self):
        for x in range(len(self._tablero)):
             for y in range(len(self._tablero[x])):
                 self._tablero[x][y] = Celda()
        self.generar_minas()

    def generar_minas(self):
        if self._coord_minas == None:
            i = {}
            for x in range(0,len(self._tablero)-1):
                a = []
                for y in range(0, len(self._tablero) - 1):
                     a[y] = y
                i[x] = a

            num_minas = 0
            while self.get_total_minas() > num_minas:
                rnd_x = randrange(len(i))
                rnd_y = randrange(len(i[rnd_x]))
                print(str(rnd_x) + "  " + str(i[rnd_x][rnd_y]) )
                print()
                self._tablero[rnd_x][i[rnd_x][rnd_y]].set_mina(True)
                i[rnd_x].pop(rnd_y)
                if len(i[rnd_x]) == 0:
                    print("ok")
                    i.pop(rnd_x)
                num_minas += 1
        else:
            for crd in self._coord_minas:
                self._tablero[crd[0]][crd[1]].set_mina(True)


    def hay_mina(self, x, y):
        return self._tablero[x][y].get_mina()

    def esta_descubierta(self, x, y):
        return self._tablero[x][y]

    def termino_el_juego(self):
        if self._celdas_restantes + self._minas_restantes == self._altura * self._anchura:
            return True
        return False

    def descubrir(self, x, y):
        self._tablero[x][y].set_descubrir(True)

    def addCelda(self, x, y):
        a = x - 1
        if a >= 0 and a < self._altura:
            b = y - 1
            if b >= 0 and b < self.get_anchura() and self._tablero[a][b].getEstado() != 3 and self._tablero[a][b].isMina():
                self.descubrir(a,b)
            b += 1
            if b >= 0 and b < self._anchura and self._tablero[a][b].getEstado() != 3 and self._tablero[a][b].isMina():
                self.descubrir(a, b)
            b += 1
            if b >= 0 and b < self._anchura and self._tablero[a][b].getEstado() != 3 and self._tablero[a][b].isMina():
                self.descubrir(a, b)

        c = x + 1
        if c >= 0 and c < self._altura:
            d = y - 1

            if d >= 0 and d < self._anchura and self._tablero[c][d].getEstado() != 3 and self._tablero[c][d].isMina():
                self.descubrir(c, d)

            d += 1
            if d >= 0 and d < self._anchura and self._tablero[c][d].getEstado() != 3 and self._tablero[c][d].isMina():
                self.descubrir(c, d)

            d += 1
            if d >= 0 and d < self._anchura and self._tablero[c][d].getEstado() != 3 and self._tablero[c][d].isMina():
                self.descubrir(c, d)


        e = y - 1
        if e >= 0 and e < self._anchura and self._tablero[x][e].getEstado() != 3 and self._tablero[x][e].isMina():
            self.descubrir(x, e)

        f = y + 1
        if f >= 0 and f < self._anchura and self._tablero[x][f].getEstado() != 3 and self._tablero[x][f].isMina():
            self.descubrir(x, f)

    def marcar_mina(self, x, y):
        self._tablero[x][y].set_marcada(True)

    def marcar_duda(self, x, y):
        self._tablero[x][y].set_descubrir(True)

    def desmarcar(self, x, y):
        self._tablero[x][y].set_marcada(False)

    def get_numero_minas_proximas(self, x, y):
        count = 0
        a = x - 1
        if a >= 0 and a < self._altura:
            b = y - 1
            if b >= 0 and b < self.get_anchura() and self._tablero[a][b].getEstado() != 3 and self._tablero[a][b].isMina():
                count += 1
            b += 1
            if b >= 0 and b < self._anchura and self._tablero[a][b].getEstado() != 3 and self._tablero[a][b].isMina():
                count += 1
            b += 1
            if b >= 0 and b < self._anchura and self._tablero[a][b].getEstado() != 3 and self._tablero[a][b].isMina():
                count += 1

        c = x + 1
        if c >= 0 and c < self._altura:
            d = y - 1

            if d >= 0 and d < self._anchura and self._tablero[c][d].getEstado() != 3 and self._tablero[c][d].isMina():
                count += 1

            d += 1
            if d >= 0 and d < self._anchura and self._tablero[c][d].getEstado() != 3 and self._tablero[c][d].isMina():
                count += 1

            d += 1
            if d >= 0 and d < self._anchura and self._tablero[c][d].getEstado() != 3 and self._tablero[c][d].isMina():
                count += 1


        e = y - 1
        if e >= 0 and e < self._anchura and self._tablero[x][e].getEstado() != 3 and self._tablero[x][e].isMina():
            count += 1

        f = y + 1
        if f >= 0 and f < self._anchura and self._tablero[x][f].getEstado() != 3 and self._tablero[x][f].isMina():
            count += 1

        return count

    def get_altura(self):
        return self._altura

    def get_anchura(self):
        return self._anchura

    def get_total_minas(self):
        return self._total_minas

    def get_minas_restantes(self):
        return self._minas_restantes

    def get_comienzo_juego(self):
        return self._comienzo_juego

    def __init__(self):
        self._altura = 10
        self._anchura = 10
        self._minas_restantes = self._total_minas = 10
        self._celdas_restantes = 0
        self._coord_minas = None
        self._comienzo_juego = "00/00/00"
        self._tablero = [[0 for x in range(self._altura)] for y in range(self._anchura)]


