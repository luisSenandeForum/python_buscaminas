class Celda:
    def __init__(self):
        self._mina = False
        self._marcada = False
        self._descubierta = False

    def get_mina(self):
        return self._mina
    def set_mina(self, mina):
        self._mina = mina

    def get_marcada(self):
        return self._marcada
    def set_marcada(self, marcada):
        self._marcada = marcada

    def get_descubierta(self):
        return self._descubierta
    def set_descubierta(self, descubierta):
        self._descubierta = descubierta